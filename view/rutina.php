
<h1 class="page-header">Rutinas</h1>

<div>
    <a href="?controller=rutina&accion=Crud">Nueva Rutina</a>
</div>

<table class="table table-striped" border=1>
    <thead>
        <tr>
            <th >Codigo</th>
            <th>ID nivel</th>
            <th>ID categoria</th>
            <th >Dias semana</th>
            <th >objetivos</th>
            <th >Descripcion</th>
        </tr>
    </thead>
    <tbody>
	<?php foreach($this->model->getAll() as $r): ?>
        <tr>
	        <td><?php echo $r->id; ?></td>
            <td><?php echo $r->id_nivel; ?></td>
            <td><?php echo $r->id_categoria; ?></td>
            <td><?php echo $r->dias_semanas; ?></td>
            <td><?php echo $r->objetivos; ?></td>
            <td><?php echo $r->descripcion; ?></td>
            <td>
                <a href="?controller=Rutina&accion=Crud&id=<?php echo $r->id ?>">Editar</a>
            </td>
            <td>
               <a href="?controller=Rutina&accion=Del&id=<?php echo $r->id ?>">Eliminar</a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table> 