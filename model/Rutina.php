<?php
include_once("database.php");

class Rutina{
    
    private $pdo;    
    public $id;
    public $id_nivel;
    public $id_categoria;
    public $dias_semanas;
    public $objetivos;
    public $descripcion;

	public function __construct(){
		try{
			$this->pdo = Database::Conectar();
		}
		catch(Exception $e){
			die($e->getMessage());
		}
	}
	public function getAll()
	{
		try{
			$result = array();
			$stm = $this->pdo->prepare("SELECT id, id_nivel, id_categoria, dias_semanas, objetivos, descripcion FROM rutina");
			$stm->execute();
			return $stm->fetchAll(PDO::FETCH_OBJ);
		}
		catch(Exception $e){
			die($e->getMessage());
		}
    }

    public function getByID($id)
    {
        try{
            $stm = $this->pdo->prepare("SELECT * FROM rutina WHERE id = ?");
                  
            $stm->execute(array($id));
            return $stm->fetch(PDO::FETCH_OBJ);
          } catch (Exception $e){
            die($e->getMessage());
        }
    }

    public function del($data){
        try{
            $stm = $this->pdo->prepare("DELETE FROM rutina WHERE id = ?");

            $stm->execute(array($data->id));
            } catch (Exception $e){
            die($e->getMessage());
          }
    }
    public function update($data)
    {
            try{
                $sql = "UPDATE rutina SET 
                id_nivel = ?, 
                id_categoria = ?,
                dias_semanas = ?, 
                objetivos = ?,
                descripcion = ?
            WHERE id = ?";

                    $this->pdo->prepare($sql)->execute(
                        array(
                            $data->id_nivel, 
                            $data->id_categoria,
                            $data->dias_semanas,
                            $data->objetivos,
                            $data->descripcion,
                            $data->id
                        )
                    );
            } catch (Exception $e){
                die($e->getMessage());
        }
    }

    public function add(Rutina $data)
    {
        try{
            $sql = "INSERT INTO rutina (id_nivel,id_categoria, dias_semanas, objetivos, descripcion) VALUES (?, ?, ?, ?, ?)";

            $this->pdo->prepare($sql)->execute(
                array(
                    $data->id_nivel, 
                    $data->id_categoria,
                    $data->dias_semanas,
                    $data->objetivos,
                    $data->descripcion
                )
            );
        } catch (Exception $e){
            die($e->getMessage());
            }
    }
}