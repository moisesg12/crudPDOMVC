/*create table miembro(
id int(5) not null auto_increment primary key,
nombre varchar(50),
apellido varchar(50),
fecha_ingreso date,
edad int (5),
direccion varchar (100),
telefono int (15),
genero varchar(10),
correo varchar (100)
);

create table nivel (
id int(5) not null auto_increment primary key,
tipo varchar (50)
);

create table categoria_rutina (
id int(5) not null auto_increment primary key,
nombre varchar (50),
descripcion varchar (50)
);

create table categoria_ejercicio(
id int(5) not null auto_increment primary key,
nombre varchar(50),
descripcion varchar(300)
);

create table cargo(
id int(5) not null auto_increment primary key,
nombre_cargo varchar(50),
descripcion varchar (50)
);
create table sala(
id int(5) not null auto_increment primary key,
nombre_sala varchar(50),
descripcon varchar(50)
);
create table empleado(
id int(5) not null auto_increment primary key,
nombre varchar(50),
id_cargo int(5),
fecha_contratacion date,
sueldo decimal (4,2),
direccion varchar (100),
telefono int (10),
foreign key(id_cargo) references cargo(id)
);

create table Empleado_sala(
id int(5) not null auto_increment primary key,
id_empleado int (5),
id_sala int (5),
foreign key(id_empleado) references empleado(id),
foreign key(id_sala) references sala(id)
);

create table ejercicio(
id int(5) not null auto_increment primary key,
nombre varchar(50),
id_catejer int (5),
id_sala int(5),
descripcion varchar(300),
foreign key(id_catejer) references categoria_ejercicio(id),
foreign key(id_sala) references sala(id)
);

create table rutina (
id int(5) not null auto_increment primary key,
id_nivel int (5),
id_categoria int(5),
dias_semanas int (50),
objetivos varchar(50),
descripcion varchar (300),
foreign key(id_nivel) references nivel(id),
foreign key(id_categoria) references categoria_rutina(id)
);

create table ejer_rutina (
id int(5) not null auto_increment primary key,
id_ejercicio int (5),
id_rutina int(5),
repeticiones int (5),
series int(5),
foreign key(id_ejercicio) references ejercicio(id),
foreign key(id_rutina) references rutina(id)
);

create table membresia (
id int(5) not null auto_increment primary key,
tipo varchar (50),
precio decimal (4,2)
);

create table suscripcion(
id int(5) not null auto_increment primary key,
id_miembro int(5) not null,
id_rutina int(5),
id_membresia int(5),
tiempo int(5),
total_pagar decimal (4,2),
foreign key(id_miembro) references miembro(id),
foreign key(id_rutina) references rutina(id),
foreign key(id_membresia) references membresia(id)
);

grant all privileges on managergym.* to 'admin';

alter table login add(
rol int(5) not null
)
foreign key(id_rutina) references rutina(id),
foreign key(id_membresia) references menbresia(id)
*/
CREATE TABLE rutina(
id INT( 5 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
id_nivel INT( 5 ) ,
id_categoria INT( 5 ) ,
dias_semanas INT( 50 ) ,
objetivos VARCHAR( 50 ) ,
descripcion VARCHAR( 300 ) /*foreign key(id_nivel) references nivel(id), foreign key(id_categoria) references categoria_rutina(id)*/
)