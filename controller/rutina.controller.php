<?php

require_once 'model/Rutina.php';

class RutinaController
{
    
    private $model;
    
    public function __construct()
    {
        $this->model = new Rutina();
    }    
    public function Index(){
        require_once 'view/rutina.php';  

    }    
    public function Crud()
    {
        $data = new Rutina();
        
        if(isset($_REQUEST['id']))
        {
            $data = $this->model->getByID($_REQUEST['id']);
        }       
        require_once 'view/rutina-editar.php';          
    } 

    public function add()
    {
        $data = new Rutina();
        
        $data->id = $_REQUEST['id'];
        $data->id_nivel = $_REQUEST['id_nivel'];
        $data->id_categoria = $_REQUEST['id_categoria'];
        $data->dias_semanas = $_REQUEST['dias_semanas'];
        $data->objetivos = $_REQUEST['objetivos'];
        $data->descripcion = $_REQUEST['descripcion'];
        $data->id > 0 
            ? $this->model->update($data)
            : $this->model->add($data);        
        header('Location: indexRutina.php');
    }
    public function Del()
    {
        $data = new Rutina();
        
         $data->id = $_REQUEST['id'];
        
        if ($data->id > 0) 
        {
            $this->model->del($data);
            header('Location: indexRutina.php');
        }else
        {
            echo "No se pudo borrar el registro";
        }
    }
}